package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "~@to-do",
        glue = "com.bdd.app",
        features = "classpath:user_management.feature",
        plugin = {"pretty", "html:target/cucumber"}
)

public class RunCukesTest {
}
